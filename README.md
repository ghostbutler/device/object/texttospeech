Text to speech
==============

Introduction
------------

This is a text to speech service that exploits the google traduction API to generate mp3 audio output
and then read it.

Dependencies
------------

For now, mp3 files are read with ffmpeg (as oto mp3 decoder do not decode mp3 v2)

So run

```bash
sudo apt install ffmpeg
```

Go dependencies:

```bash
go get github.com/evalphobia/google-tts-go/googletts
```

Use example
-----------

```bash
curl -k -H "X-Ghost-Token: `accessgenerator 127.0.0.1 GhostButler2018Scanner "GhostButlerProject2018*"`" https://127.0.0.1:16570/api/v1/controller/texttospeech -X POST -d "{\"sentence\": \"Salut comment ça va? Moi ça va. Tu fais quoi ? Réponds moi\", \"language\": \"fr\"}" -v
```

Installation
------------

On an ubuntu/debian distribution, just run the install.sh script.

When installing on raspbian, ensure you disable the volume option in the configuration file.

Add these lines into install script on raspbian

```bash
export PATH=$PATH:/usr/local/go/bin
export GOPATH=/home/pi/go
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/device/object/speechtotext.git

