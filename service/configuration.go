package service

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
)

// configuration
type Configuration struct {
	Directory struct {
		Hostname string `json:"hostname"`
	} `json:"directory"`

	SecurityManager struct {
		Hostname string `json:"hostname"`

		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"securityManager"`

	RabbitMQ struct {
		Hostname []string `json:"hostname"`
		Username string   `json:"username"`
		Password string   `json:"password"`
	} `json:"rabbitmq"`

	Storage string `json:"storage"`

	FFMpeg struct {
		IsVolumeConfigurable bool `json:"isVolumeConfigurable"`
	} `json:"ffmpeg"`
}

// build configuration
func BuildConfiguration(configurationFilePath string) (*Configuration, error) {
	// configuration
	configuration := &Configuration{}

	// load file
	if fileContent, err := ioutil.ReadFile(configurationFilePath); err == nil {
		// unmarshal
		if err := json.Unmarshal(fileContent,
			configuration); err == nil {
			configuration.Storage = strings.TrimRight(configuration.Storage,
				"/") + "/"
			configuration.Storage = os.ExpandEnv(configuration.Storage)
			_ = os.MkdirAll( configuration.Storage,
				777 )
			return configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
