package service

import (
	"encoding/json"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"io/ioutil"
	"net/http"
)

// define your services ids, must start from last built in index
const (
	// request a music load
	// must provide the source details
	// music is identified by its name
	// will download from http/ftp/sftp sources
	APIServiceV1TextToSpeechTalk = common.APIServiceType(iota + common.APIServiceBuiltInLast)

	APIServiceV1TextToSpeechTest

	// get player name
	APIServiceV1TextToSpeechGetName

	// set player name
	APIServiceV1TextToSpeechSetName
)

var MusicAPIService = map[common.APIServiceType]*common.APIEndpoint{
	APIServiceV1TextToSpeechTalk: {
		Path:                    []string{"api", "v1", "controller", "texttospeech"},
		Method:                  "POST",
		Description:             "register talk order (json language:[fr/en/...],sentence,volume)",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackTalk,
	},
	APIServiceV1TextToSpeechTest: {
		Path:                    []string{"api", "v1", "controller", "texttospeech", "test"},
		Method:                  "POST",
		Description:             "create a simple test (will say random sentence)",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackTalkTest,
	},

	APIServiceV1TextToSpeechGetName: {
		Path:                    []string{"api", "v1", "controller", "name"},
		Method:                  "GET",
		Description:             "get player name",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackGetName,
	},
	APIServiceV1TextToSpeechSetName: {
		Path:                    []string{"api", "v1", "controller", "name"},
		Method:                  "PUT",
		Description:             "set player name (name)",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackSetName,
	},
}

// talk
func CallbackTalk(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	var talk Talk
	body, _ := ioutil.ReadAll(request.Body)
	if err := json.Unmarshal(body,
		&talk); err == nil {
		if talk.Language == "" ||
			talk.Sentence == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"missing language or sentence\"}"))
			return http.StatusBadRequest
		} else {
			if talk.Volume <= 0 {
				talk.Volume = 100
			} else if talk.Volume >= 100 {
				talk.Volume = 100
			}
			fmt.Println("now queuing sentence",
				talk.Sentence,
				"with language=",
				talk.Language)
			srv.handler.NewTalkRequest(&talk)
			return http.StatusOK
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"bad json\"}"))
		return http.StatusBadRequest
	}
}

var CurrentSentence = 0
var RandomTestSentence = []string{
	"Je suis ici",
	"J'ai été activé",
	"Je suis en marche",
	"Je suis dynamique",
	"Merci de m'avoir activé",
	"Que puis-je faire pour vous?",
}

func CallbackTalkTest(_ http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	talk := &Talk{
		Volume:   100,
		Sentence: RandomTestSentence[CurrentSentence],
		Language: "fr",
	}
	if CurrentSentence+1 >= len(RandomTestSentence) {
		CurrentSentence = 0
	} else {
		CurrentSentence++
	}
	srv.handler.NewTalkRequest(talk)
	return http.StatusOK
}

// set name
func CallbackSetName(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"newName")
		if name == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"name is empty\"}"))
			return http.StatusBadRequest
		} else {
			if err := srv.handler.SaveName(name); err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				_, _ = rw.Write([]byte("{\"error\":\"failed to write name\"}"))
				return http.StatusInternalServerError
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"bad form\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

// get name
func CallbackGetName(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	rw.Header().Add("Content-Type",
		"application/json")
	_, _ = rw.Write([]byte("[\"" +
		srv.handler.Name +
		"\"]"))
	return http.StatusOK
}
