package service

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"io/ioutil"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	PlayerNameFileName = "name"
	DefaultPlayerName  = "NameIsUnset"
)

type Handler struct {
	// storage path
	storagePath string

	// volume configurable (version 3.2.12 on raspbian doesn't run with -volume option)
	isVolumeConfigurable bool

	// name
	NameMutex sync.Mutex
	Name      string

	// talk
	TalkMutex sync.Mutex
	TalkList  []*Talk

	// is running?
	isRunning bool

	// rabbit mq controller
	rabbitMQController *rabbit.Controller
}

// build handler
func BuildHandler(storagePath string,
	isVolumeConfigurable bool,
	rabbitMQController *rabbit.Controller) (*Handler, error) {
	// allocate
	handler := &Handler{
		isRunning:            true,
		isVolumeConfigurable: isVolumeConfigurable,
		rabbitMQController:   rabbitMQController,
		storagePath:          storagePath,
		TalkList:             make([]*Talk, 0, 1),
	}

	// build storage directory
	if err := os.MkdirAll(storagePath,
		0755); err != nil {
		return nil, err
	}

	// read name file
	if content, err := ioutil.ReadFile(storagePath +
		PlayerNameFileName); err == nil {
		handler.Name = strings.TrimSpace(string(content))
	}
	if handler.Name == "" {
		handler.Name = DefaultPlayerName
	}

	// run updater
	go handler.update()

	// done
	return handler, nil
}

// set player name
func (handler *Handler) SaveName(name string) error {
	handler.Name = name
	handler.NameMutex.Lock()
	defer handler.NameMutex.Unlock()
	return ioutil.WriteFile(handler.storagePath+
		PlayerNameFileName,
		[]byte(name),
		0664)
}

// record new talk request
func (handler *Handler) NewTalkRequest(talk *Talk) {
	handler.TalkMutex.Lock()
	defer handler.TalkMutex.Unlock()
	handler.TalkList = append(handler.TalkList,
		talk)
}

// update thread
func (handler *Handler) update() {
	for handler.isRunning {
		var talk *Talk = nil
		handler.TalkMutex.Lock()
		if len(handler.TalkList) > 0 {
			talk = handler.TalkList[0]
			handler.TalkList = handler.TalkList[1:]
		}
		handler.TalkMutex.Unlock()
		if talk != nil {
			if channel := handler.rabbitMQController.Channel; channel != nil {
				event := device.TTS{
					Name:     handler.Name,
					Language: talk.Language,
					Sentence: talk.Sentence,
					Volume:   talk.Volume,
				}
				if err := channel.Publish("",
					rabbit.SensorQueueName,
					false,
					false,
					amqp.Publishing{
						ContentType: "application/json",
						Body:        event.MarshalJson(),
					}); err != nil {
					fmt.Println("can't publish to rabbit:",
						err.Error())
				}
			}

			if err := talk.Talk(handler.storagePath,
				handler.isVolumeConfigurable); err != nil {
				fmt.Println(err)
			}
		}
		time.Sleep(time.Millisecond * 16)
	}
}
