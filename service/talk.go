package service

import (
	"errors"
	"github.com/evalphobia/google-tts-go/googletts"
	"gitlab.com/ghostbutler/tool/service"
	"io"
	"net/http"
	"os"
	"os/exec"
	"strconv"
)

type Talk struct {
	Sentence string `json:"sentence"`
	Language string `json:"language"`
	Volume   int    `json:"volume"`
}

func (talk *Talk) Talk(storagePath string,
	isVolumeConfigurable bool) error {
	if url, err := googletts.GetTTSURL(talk.Sentence,
		talk.Language); err == nil {
		if response, err := http.DefaultClient.Get(url); err == nil {
			defer common.Close(response.Body)
			if response.StatusCode == http.StatusOK {
				path := storagePath +
					"tts"
				if f, err := os.Create(path); err == nil {
					defer common.Close(f)
					_, _ = io.Copy(f,
						response.Body)
					var c *exec.Cmd
					if isVolumeConfigurable {
						c = exec.Command("ffplay",
							path,
							"-nodisp",
							"-loglevel",
							"quiet",
							"-volume",
							strconv.Itoa(talk.Volume),
							"-autoexit")
					} else {
						c = exec.Command("ffplay",
							path,
							"-nodisp",
							"-loglevel",
							"quiet",
							"-autoexit")
					}
					return c.Run()
				} else {
					return err
				}
			} else {
				return errors.New("request failed")
			}
		} else {
			return err
		}
	} else {
		return err
	}
}
